El corpus Mensajes_director_cine_investigados es un conjunto de 50 supuestos mensajes enviados a un director de cine, opinando sobre su última película.
Los mensajes están totalmente inventados.

El corpus tiene tres columnas: persona que envía el mensaje, mensaje y calificación del mensaje (positivo, negativo, y amenazante).


