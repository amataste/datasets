# datasets

Datasets públicos

Grupos de datasets usados con carácter formativo. Se pueden utilizar para ejemplificar procesos de investigación y análisis.
Debe tenerse en cuenta que los datos pueden estar inventados, simulados, o parcialmente tomados de datasets públicos reales. Por tanto, no deberían utilizarse para investigaciones.